<?php

declare(strict_types=1);

namespace Hgraca\ComposerPlugin;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\Installer\PackageEvent;
use Composer\Installer\PackageEvents;
use Composer\IO\IOInterface;
use Composer\Plugin\CommandEvent;
use Composer\Plugin\PluginEvents;
use Composer\Plugin\PluginInterface;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;

final readonly class Plugin implements PluginInterface, EventSubscriberInterface
{
    private Composer $composer;

    private IOInterface $io;

    public function activate(Composer $composer, IOInterface $io): void
    {
        $this->composer = $composer;
        $this->io = $io;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            PackageEvents::POST_PACKAGE_INSTALL => 'onEvent',
            PackageEvents::POST_PACKAGE_UPDATE => 'onEvent',
            PackageEvents::POST_PACKAGE_UNINSTALL => 'onEvent',
            PluginEvents::COMMAND => 'onEvent',
            ScriptEvents::POST_AUTOLOAD_DUMP => 'onEvent',
            ScriptEvents::POST_INSTALL_CMD => 'onEvent',
            ScriptEvents::POST_UPDATE_CMD => 'onEvent',
        ];
    }

    public function onEvent($event): void
    {
        $this->io->write(__METHOD__);
        $this->io->write('Event ' . get_class($event) . ', with name ' . $event->getName());
    }

    public function deactivate(Composer $composer, IOInterface $io): void
    {
    }

    public function uninstall(Composer $composer, IOInterface $io): void
    {
    }
}
