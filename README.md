# Project to try out a composer plugin

This project consists of 2 test repos, and a plugin repo, plus a test repo to install 
the previous 3 and see how the plugin works, or not.

## The problem

The plugin simply listens to events and writes in the CLI the plugin method that 
was called and the event that was received.

It seems like composer tries to load the same plugin twice, which results in failing to run some commands, 
ie `remove` and `require`.

## How to replicate the problem

1. Clone this repository;
2. in the project root run
   ```
   ./composer.phar -d ./TestProject install
   ./composer.phar -d ./TestProject remove "hgraca/dummy-project-1"
   ./composer.phar -d ./TestProject require hgraca/dummy-project-1:dev-master
   ``` 

You will see that the last 2 commands will reply with error messages:

```
$ ./composer.phar -d ./TestProject install
Installing dependencies from lock file (including require-dev)
Verifying lock file contents can be installed on current platform.
Package operations: 3 installs, 0 updates, 0 removals
  - Installing hgraca/composer-plugin (dev-master): Symlinking from ../composer-plugin
Hgraca\ComposerPlugin\Plugin::onEvent
Event Composer\Installer\PackageEvent, with name post-package-install
  - Installing hgraca/dummy-project-1 (dev-master): Symlinking from ../DummyProject1
  - Installing hgraca/dummy-project-2 (dev-master): Symlinking from ../DummyProject2
Hgraca\ComposerPlugin\Plugin::onEvent
Event Composer\Installer\PackageEvent, with name post-package-install
Hgraca\ComposerPlugin\Plugin::onEvent
Event Composer\Installer\PackageEvent, with name post-package-install
Generating autoload files
Hgraca\ComposerPlugin\Plugin::onEvent
Event Composer\Script\Event, with name post-autoload-dump
Hgraca\ComposerPlugin\Plugin::onEvent
Event Composer\Script\Event, with name post-install-cmd
```
```
$ ./composer.phar -d ./TestProject remove "hgraca/dummy-project-1"
./composer.json has been updated
PHP Fatal error:  Cannot declare class Hgraca\ComposerPlugin\Plugin, because the name is already in use in phar:///home/herberto/Development/hgraca/php-composer-shell-plugin-test/composer.phar/src/Composer/Plugin/PluginManager.php(274) : eval()'d code on line 18

Fatal error: Cannot declare class Hgraca\ComposerPlugin\Plugin, because the name is already in use in phar:///home/herberto/Development/hgraca/php-composer-shell-plugin-test/composer.phar/src/Composer/Plugin/PluginManager.php(274) : eval()'d code on line 18
```
```
$ ./composer.phar -d ./TestProject require hgraca/dummy-project-1:dev-master
Info from https://repo.packagist.org: #StandWithUkraine
./composer.json has been updated
PHP Fatal error:  Cannot declare class Hgraca\ComposerPlugin\Plugin, because the name is already in use in phar:///home/herberto/Development/hgraca/php-composer-shell-plugin-test/composer.phar/src/Composer/Plugin/PluginManager.php(274) : eval()'d code on line 18

Fatal error: Cannot declare class Hgraca\ComposerPlugin\Plugin, because the name is already in use in phar:///home/herberto/Development/hgraca/php-composer-shell-plugin-test/composer.phar/src/Composer/Plugin/PluginManager.php(274) : eval()'d code on line 18
herberto@CM-NOTEBOOK-649:~/Development/hgraca/php-composer-shell-plugin-test$
```